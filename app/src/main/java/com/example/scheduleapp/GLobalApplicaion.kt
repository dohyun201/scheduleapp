package com.example.scheduleapp

import android.app.Application
import android.content.Context
import com.example.scheduleapp.Kakao.KakaoSDKAdapter
import com.google.firebase.FirebaseApp
import com.kakao.auth.*

class GLobalApplicaion :Application(){

    companion object{
        lateinit var instance : GLobalApplicaion
        private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        // 카카오톡 초기화
        KakaoSDK.init(KakaoSDKAdapter())

    }

    fun context(): Context = applicationContext
}