package com.example.scheduleapp.Util

import android.content.SharedPreferences

class SharedApp {
    fun setData(pref: SharedPreferences?, key: String, value: Boolean) {
        var editor : SharedPreferences.Editor = pref!!.edit()
        editor.putBoolean(key,value)
        editor.commit()
    }

    fun getData(pref:SharedPreferences?, key: String): Boolean {
        return pref!!.getBoolean(key,false)
    }
}
