package com.example.scheduleapp.View.Login

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.scheduleapp.Data.GuestItem
import com.example.scheduleapp.Data.KakaoItem
import com.example.scheduleapp.View.MainFragment
import com.example.scheduleapp.R
import com.example.scheduleapp.Util.Util
import com.example.scheduleapp.View.BasicPopup
import com.example.scheduleapp.View.MainActivity
import com.example.scheduleapp.retrofit.request.APIInterface
import com.example.scheduleapp.retrofit.response.NormalResponse
import com.example.scheduleapp.retrofit.response.StoreResponse
import com.example.scheduleapp.retrofit.requestModel.StoresModel
import com.kakao.util.helper.log.Logger
import kotlinx.android.synthetic.main.login_extra_client_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginExtraFragmentClient : Fragment(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        material_text_button.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                if(!input_code_txt.editText!!.text.isEmpty()){
                    Logger.e(input_code_txt.editText!!.text.toString())
                    var storeCode = input_code_txt.editText!!.text.toString().toLong()
                    checkCode(StoresModel(storeCode, ""))
                }
            }
        })
    }

    private fun getRetrofit(): APIInterface {
        return MainActivity.retrofitClient.create(APIInterface::class.java)
    }

    private fun checkCode(model: StoresModel){
        var hasStore = false

        getRetrofit().getStoreList(model).enqueue(object : Callback<StoreResponse> {
            override fun onFailure(call: Call<StoreResponse>, t: Throwable) {
                Log.e("Throwable22===>", t.message)
            }

            override fun onResponse(call: Call<StoreResponse>, response: Response<StoreResponse>) {
                Log.e("response22===>", response.body().toString())

                for(stores in response.body()!!.data){
                    if(stores.storeCode.equals(model.storeCode))
                    {
                        hasStore = true
                        model.storeName = stores.storeName
                    }
                }

                if(hasStore){
                    var kakao = KakaoItem().getData()
                    Log.e("kakao====>", kakao.toString())
                    // store 데이터 있음 -> 회원등록 되고 -> 메인이동
                    var guestCode = kakao!!.id
                    var nickName = kakao!!.nickname
                    var email = kakao!!.kakaoAccount.email
                    var ageRange = kakao!!.kakaoAccount.ageRange
                    var gender = kakao!!.kakaoAccount.gender

                    Log.e("nickName====>", nickName)

                    Log.e("model====>", model.toString())

                    addNewGuest(model,GuestItem(guestCode,model.storeCode,nickName,email,ageRange,gender))
                    //goMainfragment()
                }else{
                    // store 데이터 없음 -> 팝업
                    Log.e("다른것 입력 요청","다른내용 입력")
                    BasicPopup(activity, Util().MESSAGE_00).show()
                }
            }
        })
    }

    private fun addNewGuest(model: StoresModel, modelGuest: GuestItem) {
        getRetrofit().addNewGuest(modelGuest).enqueue(object : Callback<NormalResponse> {
            override fun onFailure(call: Call<NormalResponse>, t: Throwable) {
                Log.e("Throwable22===>", t.message)
            }

            override fun onResponse(call: Call<NormalResponse>, response: Response<NormalResponse>) {
                Log.e("response22===>", response.body().toString())
                if(response.isSuccessful){
                    goMainfragment(model)
                }
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.login_extra_client_layout,container,false)
    }

    private fun goMainfragment(model: StoresModel) {
        var fragment = MainFragment()

        var bundle = Bundle()
        bundle.putString("nickName",arguments!!.getString("nickName"))
        bundle.putLong("storeCode",model.storeCode)
        bundle.putString("storeName",model.storeName)
        bundle.putInt("type",2) // 2 - client

        fragment.arguments = bundle

        activity!!.supportFragmentManager
            .beginTransaction()
            .replace(R.id.root,fragment)
            .addToBackStack(null)
            .commit()
    }

}