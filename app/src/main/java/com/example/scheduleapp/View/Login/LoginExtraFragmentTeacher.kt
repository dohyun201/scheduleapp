package com.example.scheduleapp.View.Login

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.scheduleapp.View.MainActivity
import com.example.scheduleapp.View.MainFragment
import com.example.scheduleapp.R
import com.example.scheduleapp.retrofit.request.APIInterface
import com.example.scheduleapp.retrofit.response.StoreResponse
import com.example.scheduleapp.retrofit.requestModel.StoresModel
import kotlinx.android.synthetic.main.login_extra_client_layout.input_code_txt
import kotlinx.android.synthetic.main.login_extra_client_layout.material_text_button
import kotlinx.android.synthetic.main.login_extra_teacher_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginExtraFragmentTeacher : androidx.fragment.app.Fragment(){

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getRetrofit()

        material_text_button.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                com.kakao.util.helper.log.Logger.e(input_code_txt.editText!!.text.toString())
                com.kakao.util.helper.log.Logger.e(input_store_name_txt.editText!!.text.toString())
                var storeCode = input_code_txt.editText!!.text.toString().toLong()
                var storeName = input_store_name_txt.editText!!.text.toString()

                checkStoreCode(StoresModel(storeCode, storeName))
            }
        })
    }

    private fun getRetrofit(): APIInterface {
        return MainActivity.retrofitClient.create(APIInterface::class.java)
    }

    private fun checkStoreCode(model: StoresModel) : Boolean {

        var hasStore = false

        getRetrofit().getStoreList(model).enqueue(object : Callback<StoreResponse> {
            override fun onFailure(call: Call<StoreResponse>, t: Throwable) {
                Log.e("Throwable22===>", t.message)
            }

            override fun onResponse(call: Call<StoreResponse>, response: Response<StoreResponse>) {
                Log.e("response22===>", response.body().toString())

                for(stores in response.body()!!.data){
                    if(stores.storeCode.equals(model.storeCode) || stores.storeName.equals(model.storeName))
                    {
                        hasStore = true
                    }
                }

                if(hasStore){
                    // store 데이터 있음 -> 팝업
                    Log.e("다른것 입력 요청","다른내용 입력")
                    // 선생님들 가입시 고려해야함
                    goMainfragment(model)
                }else{
                    // store 데이터 없음, 새로 등록
                    setNewStore(model)
                }
            }
        })

        // 이미 데이터 있는지 확인, 없으면 세팅
//
        return hasStore
    }

    fun setNewStore(model :StoresModel){
        getRetrofit().setNewStore(model).enqueue(object : Callback<StoresModel> {
            override fun onFailure(call: Call<StoresModel>, t: Throwable) {
                Log.e("Throwable22===>", t.message)
            }

            override fun onResponse(call: Call<StoresModel>, response: Response<StoresModel>) {
                Log.e("response22===>", response.body().toString())
                // 신규등록후 메인화면 이동
                goMainfragment(model)
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.login_extra_teacher_layout,container,false)
    }

    private fun goMainfragment(model: StoresModel) {
        var fragment = MainFragment()

        var bundle = Bundle()
        bundle.putString("nickName",arguments!!.getString("nickName"))
        bundle.putLong("storeCode",model.storeCode)
        bundle.putString("storeName",model.storeName)
        bundle.putInt("type",1) // 1 - teacher , 2- client

        fragment.arguments = bundle

        activity!!.supportFragmentManager
            .beginTransaction()
            .replace(R.id.root,fragment)
            .addToBackStack(null)
            .commit()
    }

}