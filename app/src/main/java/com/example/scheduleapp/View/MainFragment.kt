package com.example.scheduleapp.View

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.scheduleapp.Adapter.ScheduleAdapter
import com.example.scheduleapp.Kakao.SessionCallback
import com.example.scheduleapp.R
import com.example.scheduleapp.Data.SchedulItem
import com.example.scheduleapp.Data.kakaoData
import com.example.scheduleapp.retrofit.request.APIInterface
import com.example.scheduleapp.retrofit.request.APIRequest
import com.example.scheduleapp.retrofit.request.RetrofitDataResponse
import com.example.scheduleapp.retrofit.requestModel.AttendModel
import com.example.scheduleapp.retrofit.requestModel.StoresModel
import com.example.scheduleapp.retrofit.requestModel.LectureModel
import com.example.scheduleapp.retrofit.response.AttendlistResponse
import com.example.scheduleapp.retrofit.response.ClassListResponse
import com.example.scheduleapp.retrofit.response.NormalResponse
import com.kakao.util.helper.log.Logger
import com.squareup.picasso.Picasso
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import kotlinx.android.synthetic.main.main_fragment_layout.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList


class MainFragment :Fragment(), com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {


    var context : Fragment = this
    var classCode: Int = 0

    var schedulelist : ArrayList<SchedulItem> = ArrayList()
    var TEACHER = 1
    var CLIENT = 2
    var seleted_days:String = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        "0"+LocalDate.now().dayOfMonth.toString()
    } else {
        Calendar.DAY_OF_MONTH.toString()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.main_fragment_layout,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var arg = arguments

        Logger.e(seleted_days)

        // 메인화면 프로필 세팅
        profileInit(arg)

//        calendarInit()
        initView(arg)

        // 클래스 목록 세팅
        classListInit(arg)

        clickEvent()
    }

    private fun initView(arg: Bundle?) {

        Log.e("arg===>", arg.toString())

        store_name.text = arg!!.getString("storeName")

        if(arg!!.getInt("type") == TEACHER){
            //선생

        }else{
            //학생
            floating_action_button.visibility = View.GONE
        }
    }

    private fun classListInit(arg: Bundle?) {

        var storeCode = arg!!.getLong("storeCode")
        // 학생 선생 구분자
        var type = arg!!.getInt("type")

        // 수업정보 가져오기
        APIRequest(getRetrofit()).getClassList(type,StoresModel(storeCode,""), object : RetrofitDataResponse{
            override fun responseResult(body: NormalResponse?) {
            }

            override fun responseAttendListResult(body: AttendlistResponse?) {
            }

            override fun responseClassListResult(response: ClassListResponse?) {
                Log.e("responseResult=>","getClassList11 ===> "+ response)
                //어뎁터 리스트에 매핑 , 레트로핏도 전달
                if(response!=null){

                    if(type.equals(CLIENT)){
                        //수업 참여 여부 확인
                        checkAttendClass(response)
                    }

                    scheduleList.adapter = ScheduleAdapter(context,type,getRetrofit(),response!!.data)
                    classCode = response!!.data.size
                }
            }
        })

//        schedulelist.add(SchedulItem(seleted_days,"","수업1","About Class", "Des fiches de questions qui couvrent des ouvrages complets chapitre par chapitre. Les quizz Harry Potter sont l'oeuvre d'élèves du 12/14 rue d'Alésia. Les autres sont des adaptations de questionnaires réalisés par divers enseignants pour CLS, programme mis au point par Jean-Marc Campaner, qui m'a très gentiment autorisé à e"))
//        scheduleList.adapter = ScheduleAdapter(schedulelist)

    }

    private fun checkAttendClass(model: ClassListResponse){

        for(modeldata in model.data){
            APIRequest(getRetrofit()).getAttendCount(AttendModel(
                modeldata.classCode+"A"+kakaoData!!.id,
                modeldata.className,
                modeldata.storeCode
            ), object : RetrofitDataResponse {
                override fun responseAttendListResult(body: AttendlistResponse?) {
//                    Log.e("responseData===>", body.toString())
//                    Log.e("참석여부===>", body!!.data.size.toString())
                    var compareId = modeldata.classCode+"A"+kakaoData!!.id
//                    Log.e("compareId===>", compareId)

                    for(data in body!!.data){
//                        Log.e("data.lectureCode===>", data.lectureCode)
                        if(compareId.equals(data.lectureCode)){
                            Log.e("참여함!===>", "참여함!")
                        }
                    }
                }

                override fun responseResult(body: NormalResponse?) {
                    Log.e("checkAtd 222===>", body.toString())
                }

                override fun responseClassListResult(body: ClassListResponse?) {
                    Log.e("responseClassList===>", body.toString())
                }
            })
        }
    }

    private fun getRetrofit(): APIInterface {
        return MainActivity.retrofitClient.create(APIInterface::class.java)
    }


    private fun profileInit(arg: Bundle?) {
        Logger.e(arg.toString()+"====>bundle!!!")

        Picasso.get()
            .load(SessionCallback.profile_image)
            .into(profile_image)

        profile_nickname.text = arg!!.getString("nickName")
    }

    private fun clickEvent() {
        floating_action_button.setOnClickListener(object:View.OnClickListener{
            override fun onClick(view: View?) {
                // 선생님 입장에서 수업 추가
                addClassTime()
            }
        })
    }

    private fun addClassTime() {
        // 수업 생성 팝업 띄우기
        var storeCode = arguments!!.getLong("storeCode")
        var type = arguments!!.getInt("type")

        BasicInputPopup(
            scheduleList.adapter as ScheduleAdapter
            ,getRetrofit()
            ,context
            ,LectureModel(
                "C"+(classCode+1),
                "",
                "",
                "",
                0,
                0,
                storeCode
                ),
            "add"
        ).show()

//        // 타임픽커 호출
//        var now : Calendar = Calendar.getInstance()
//        // 디폴트 값
//        var tpd : TimePickerDialog = TimePickerDialog.newInstance(
//            this,
//            12,
//            0,
//            true
//        )
//
//        tpd.show(requireFragmentManager(),"timePickerDialog")
//        tpd.version = TimePickerDialog.Version.VERSION_1
    }


    private fun calendarInit() {
        var now : Calendar = Calendar.getInstance()
        var dpd : com.wdullaer.materialdatetimepicker.date.DatePickerDialog = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
            this,
            now.get(Calendar.YEAR),
            now.get(Calendar.MONTH),
            now.get(Calendar.DAY_OF_MONTH)
        )

        dpd.show(requireFragmentManager(),"dataPickerDialog")
        dpd.version = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.Version.VERSION_1
    }

    override fun onDateSet(
        view: com.wdullaer.materialdatetimepicker.date.DatePickerDialog?,
        year: Int,
        monthOfYear: Int,
        dayOfMonth: Int
    ) {
        Logger.e("view==>"+view)
        Logger.e("year==>"+year)
        Logger.e("monthOfYear==>"+monthOfYear) // 월
        Logger.e("dayOfMonth==>"+dayOfMonth) // 일

        seleted_days = "0"+dayOfMonth

        // 월은 1월이 0 이므로 , +1 해야함
        var convertMonthOfYear = monthOfYear+1
        Logger.e("convertMonthOfYear==>"+convertMonthOfYear) // 월
        //요일 구하기
        var dayOfweek = getDayofWeek(year.toString(),convertMonthOfYear.toString(),dayOfMonth.toString())

        //갱신
        schedulelist.clear()

        addItem(
            SchedulItem(
                seleted_days,
                dayOfweek,
                "수업1",
                "About Class",
                "Des fiches de questions qui couvrent des ouvrages complets chapitre par chapitre. Les quizz Harry Potter sont l'oeuvre d'élèves du 12/14 rue d'Alésia. Les autres sont des adaptations de questionnaires réalisés par divers enseignants pour CLS, programme mis au point par Jean-Marc Campaner, qui m'a très gentiment autorisé à e"
            )
        )
//        schedulelist.add(SchedulItem(seleted_days,"SUN","수업"))
//        scheduleList.adapter!!.notifyDataSetChanged()
    }

    private fun getDayofWeek(year: String, monthOfYear: String, dayOfMonth: String): String {
        var convertMonthOfYear = ""
        var convertDayOfMonth = ""

        if(monthOfYear.length == 1){
            convertMonthOfYear = "0"+monthOfYear
        }else{
            convertMonthOfYear = monthOfYear
        }

        if(dayOfMonth.length == 1){
            convertDayOfMonth = "0"+dayOfMonth
        }else{
            convertDayOfMonth = dayOfMonth
        }


        var dayofWeek = year+convertMonthOfYear+convertDayOfMonth
        var dataFormat = SimpleDateFormat("yyyyMMdd")
        var date = dataFormat.parse(dayofWeek.toString())

        var calendar = Calendar.getInstance()
        calendar.time = date
        var dayOfWeekIndex = calendar.get(Calendar.DAY_OF_WEEK)
        Log.e("dayOfWeekIndex==>", dayOfWeekIndex.toString())
        var dayOfWeekStr = arrayListOf("SUN","MON","TUES","WED","THURS","FRI","SAT")

        return dayOfWeekStr[dayOfWeekIndex-1]
    }

    override fun onTimeSet(view: TimePickerDialog?, hourOfDay: Int, minute: Int, second: Int) {
        // 타임피커에 대한 세팅 값
        Logger.e("view==>"+view)
        Logger.e("hourOfDay==>"+hourOfDay) // 시
        Logger.e("minute==>"+minute) // 분

//        var convertMinute = ""
//
//        if(minute.toString().length == 1){
//            convertMinute = "0"+minute
//        }else{
//            convertMinute = minute.toString()
//        }
//
//        addItem(
//            SchedulItem(
//                "",
//                "",
//                hourOfDay.toString() + ":" + convertMinute + "PM 수업",
//                "Class" + hourOfDay.toString() + ":" + convertMinute,
//                convertMinute + "분 수업"
//            )
//        )

    }


    override fun onResume() {
        super.onResume()

        //데이터 다시 부름
        classListInit(arguments)

        //갱신
        if(schedulelist.size>0){
            scheduleList.adapter!!.notifyDataSetChanged()
        }
    }

    fun addItem(schedulItem: SchedulItem) {
        schedulelist.add(schedulItem)
        scheduleList.adapter!!.notifyDataSetChanged()
    }
}