package com.example.scheduleapp.View

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import androidx.fragment.app.FragmentActivity
import com.example.scheduleapp.R
import kotlinx.android.synthetic.main.dialog_layout.*
import kotlinx.android.synthetic.main.dialog_layout.view.*

class BasicPopup(context: FragmentActivity?, var message: String) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE) // 타이틀바 업앰
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) // 배경 투명
        setContentView(R.layout.dialog_layout)

        dialog_text.text = message

        dialog_confirm.setOnClickListener(object :View.OnClickListener{
            override fun onClick(p0: View?) {
                dismiss()
            }
        })
    }

}