package com.example.scheduleapp.View

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.scheduleapp.Kakao.SessionCallback
import com.example.scheduleapp.View.Login.LoginExtraFragmentClient
import com.example.scheduleapp.View.Login.LoginExtraFragmentTeacher
import com.example.scheduleapp.R
import com.example.scheduleapp.Util.SharedApp
import com.example.scheduleapp.retrofit.init.APIClient
import com.kakao.auth.Session
import com.kakao.usermgmt.UserManagement
import com.kakao.usermgmt.callback.LogoutResponseCallback
import com.kakao.usermgmt.response.MeV2Response
import com.kakao.util.helper.log.Logger
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Retrofit

var savedInstanceStated : Bundle? = null

class MainActivity : AppCompatActivity(){

    lateinit var callback : SessionCallback
    lateinit var mMainFragment: MainFragment
    lateinit var mLoginFragment_client: LoginExtraFragmentClient
    lateinit var mLoginFragment_teacher: LoginExtraFragmentTeacher
    var apiClient : APIClient = APIClient()

    companion object{
        lateinit var mainInstance : MainActivity
        var selected_client : Boolean = false
        var selected_teacher : Boolean = false
        lateinit var retrofitClient : Retrofit
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceStated = savedInstanceState
        setContentView(R.layout.activity_main)
        mainInstance = this

        callback = SessionCallback()

        // 콜백 초기화
        kakaoCallbackInit(callback)

        // 레트로핏 초기화
        initRetorifit()

        // 클릭 이벤트
        clickEvent()
//        kakao_logout.setOnClickListener(object: View.OnClickListener {
//            override fun onClick(p0: View?) {
//                LogOut()
//            }
//        })

    }

    private fun getSharedPreference(): SharedPreferences? {
        return getSharedPreferences("pref", Context.MODE_PRIVATE)
    }

    private fun initRetorifit() {
        // 레트로핏 초기화
        retrofitClient = apiClient.getClient()
    }

    private fun clickEvent() {
        // client 선택시
        select_client.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                select_client.borderColor = Color.parseColor("#ff867c")
                select_teacher.borderColor = Color.parseColor("#FF000000")
                select_client_txt.setTextColor(Color.parseColor("#ff867c"))
                select_teacher_txt.setTextColor(Color.parseColor("#FF000000"))

                selected_teacher = false
                selected_client = true

                SharedApp().setData(getSharedPreference(),"selected_client",selected_client)

            }
        })
        // teacher 선택시
        select_teacher.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                select_client.borderColor = Color.parseColor("#FF000000")
                select_teacher.borderColor = Color.parseColor("#ff867c")
                select_client_txt.setTextColor(Color.parseColor("#FF000000"))
                select_teacher_txt.setTextColor(Color.parseColor("#ff867c"))

                selected_teacher = true
                selected_client = false

                SharedApp().setData(getSharedPreference(),"selected_teacher",selected_teacher)

            }
        })
    }

    private fun LogOut() {
        UserManagement.getInstance().requestLogout(object: LogoutResponseCallback(){
            override fun onCompleteLogout() {
                Logger.e("로그아웃완료!")
            }
        })
    }

    private fun kakaoCallbackInit(callback: SessionCallback) {
        Session.getCurrentSession().addCallback(callback)
        Session.getCurrentSession().checkAndImplicitOpen()
    }

    fun goMainFragment(kakaoResponse: MeV2Response?) {
        if(savedInstanceStated ==null){
            root.visibility = View.VISIBLE
//            var args = Bundle()

            var bundle = Bundle()
            bundle.putString("nickName",kakaoResponse!!.nickname)


            mMainFragment = MainFragment()
            mMainFragment.arguments = bundle

            supportFragmentManager
                .beginTransaction()
                .add(R.id.root,mMainFragment)
                .commit()
        }else{
            var fragment : Fragment? = supportFragmentManager.findFragmentById(R.id.root)
            mMainFragment = fragment as MainFragment
        }

    }

    fun goLoginExtraFragment(kakaoResponse: MeV2Response?) {
        if(savedInstanceStated == null){

            root.visibility = View.VISIBLE
            var bundle = Bundle()

            bundle.putString("nickName",kakaoResponse!!.nickname)

           var selected_client = SharedApp().getData(getSharedPreference(),"selected_client")

            if(selected_client) {
                mLoginFragment_client = LoginExtraFragmentClient()
                mLoginFragment_client.arguments = bundle

                supportFragmentManager
                    .beginTransaction()
                    .add(R.id.root,mLoginFragment_client)
                    .commitAllowingStateLoss()
            }else{
                mLoginFragment_teacher = LoginExtraFragmentTeacher()
                mLoginFragment_teacher.arguments = bundle

                supportFragmentManager
                    .beginTransaction()
                    .add(R.id.root,mLoginFragment_teacher)
                    .commitAllowingStateLoss()
            }

        }else{
            var fragment : Fragment? = supportFragmentManager.findFragmentById(R.id.root)
            if(selected_client){
                mLoginFragment_client = fragment as LoginExtraFragmentClient
            }else{
                mLoginFragment_teacher = fragment as LoginExtraFragmentTeacher
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(Session.getCurrentSession().handleActivityResult(requestCode,resultCode,data)){
            Logger.e("onActivityResult=>"+requestCode)
            Logger.e("onActivityResult=>"+resultCode)
            Logger.e("onActivityResult=>"+data.toString())
            return
        }

        super.onActivityResult(requestCode, resultCode, data)
    }


    override fun onDestroy() {
        super.onDestroy()
        Session.getCurrentSession().removeCallback(callback)
    }

}
