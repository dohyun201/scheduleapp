package com.example.scheduleapp.View

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.NumberPicker
import androidx.fragment.app.Fragment
import com.example.scheduleapp.Adapter.ScheduleAdapter
import com.example.scheduleapp.R
import com.example.scheduleapp.retrofit.request.APIInterface
import com.example.scheduleapp.retrofit.request.APIRequest
import com.example.scheduleapp.retrofit.request.RetrofitDataResponse
import com.example.scheduleapp.retrofit.requestModel.LectureModel
import com.example.scheduleapp.retrofit.response.AttendlistResponse
import com.example.scheduleapp.retrofit.response.ClassListResponse
import com.example.scheduleapp.retrofit.response.NormalResponse
import com.kakao.util.helper.log.Logger
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import kotlinx.android.synthetic.main.dialog_input_layout.*
import java.util.*

class BasicInputPopup(
    var adapter: ScheduleAdapter,
    var retrofit: APIInterface,
    var fragment: Fragment,
    var classModel: LectureModel,
    var inputType: String
    ) : Dialog(fragment.context), com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE) // 타이틀바 업앰
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) // 배경 투명
        setContentView(R.layout.dialog_input_layout)

        initPicker()

        dialog_input_confirm.setOnClickListener(object :View.OnClickListener{
            override fun onClick(p0: View?) {
                // 타임픽커 호출
                showTimePicker()
            }
        })

        dialog_input_cancel.setOnClickListener(object :View.OnClickListener{
            override fun onClick(p0: View?) {
                dismiss()
            }
        })
    }

    private fun showTimePicker() {
        // 디폴트 값
        var tpd : TimePickerDialog = TimePickerDialog.newInstance(
            this,
            12,
            0,
            true
        )

        tpd.show(fragment.requireFragmentManager(),"timePickerDialog")
        tpd.version = TimePickerDialog.Version.VERSION_1
    }

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {

    }

    override fun onTimeSet(view: TimePickerDialog?, hourOfDay: Int, minute: Int, second: Int) {
        // 타임피커에 대한 세팅 값
        Logger.e("hourOfDay==>"+hourOfDay) // 시
        Logger.e("minute==>"+minute) // 분
        if(inputType == "add"){
            addNewdClass(convertTime(hourOfDay),convertTime(minute))
        }else{
            modifyClass()
        }
    }

    private fun convertTime(time: Int): String {
        var convertTime = ""
        //1자리면 앞에 0을 붙인다
        if(time.toString().length == 1){
            convertTime = "0"+time
        }else{
            convertTime = time.toString()
        }
        return convertTime
    }

    private fun addNewdClass(hourOfDay: String, minute: String) {
        // 수업정보 가져오기

        APIRequest(retrofit).addClass(LectureModel(
            classModel.classCode,
            dialog_text_content.text.toString(),
            dialog_text_title.text.toString(),
            getCuurentDate()+hourOfDay+minute,
            number_picker.value,
            classModel.attendCount,
            classModel.storeCode
        ), object : RetrofitDataResponse{
            override fun responseAttendListResult(body: AttendlistResponse?) {
            }

            override fun responseResult(body: NormalResponse?) {
                Log.e("responseResult=>","addClass ===> "+ body)
                //TODO 강의 개설여부 푸시 알림

                fragment.onResume()
                // 화면 갱신
//                adapter.notifyDataSetChanged()
                // 개설된 강의 다시 받아와서 세팅
                //classListInit(arguments)
            }

            override fun responseClassListResult(response: ClassListResponse?) {
            }
        })

        dismiss()
    }

    private fun getCuurentDate() :String{
        var calendar : Calendar = Calendar.getInstance()
        var year = calendar.get(Calendar.YEAR)
        var month = calendar.get(Calendar.MONTH)
        var day = calendar.get(Calendar.DAY_OF_MONTH)
        Log.e("year", year.toString())
        Log.e("month", (month+1).toString())
        Log.e("day", day.toString())

        return  year.toString()+(month+1).toString()+day.toString()
    }

    private fun modifyClass() {

        // 수정된 데이터로 세팅
        classModel.className = dialog_text_title.text.toString()
        classModel.aboutClass = dialog_text_content.text.toString()
        classModel.lectureCount = number_picker.value


        APIRequest(retrofit).modifyClass(classModel, object : RetrofitDataResponse {
            override fun responseAttendListResult(body: AttendlistResponse?) {
            }

            override fun responseClassListResult(body: ClassListResponse?) {}

            override fun responseResult(body: NormalResponse?) {
                // 화면 갱신
                // adapter.notifyDataSetChanged()
            }
        })

        adapter.notifyDataSetChanged()
        dismiss()
    }

    private fun initPicker() {
        number_picker.maxValue = 100
        number_picker.minValue = 1
        // 100까지 가면 다시 0으로
        number_picker.wrapSelectorWheel = true
        // edit text 로 수정하지 못하게
        number_picker.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS

        //원래 쓰여진 데이터 세팅
        dialog_text_title.text = Editable.Factory.getInstance().newEditable(classModel.className)
        dialog_text_content.text = Editable.Factory.getInstance().newEditable(classModel.aboutClass)
    }

}