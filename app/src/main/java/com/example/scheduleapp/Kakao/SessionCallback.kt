package com.example.scheduleapp.Kakao

import com.example.scheduleapp.Data.KakaoItem
import com.example.scheduleapp.View.MainActivity.Companion.mainInstance
import com.kakao.auth.AccessTokenCallback
import com.kakao.auth.ISessionCallback
import com.kakao.auth.Session
import com.kakao.auth.authorization.accesstoken.AccessToken
import com.kakao.network.ErrorResult
import com.kakao.usermgmt.UserManagement
import com.kakao.usermgmt.callback.MeV2ResponseCallback
import com.kakao.usermgmt.response.MeV2Response
import com.kakao.util.OptionalBoolean
import com.kakao.util.exception.KakaoException
import com.kakao.util.helper.log.Logger

class SessionCallback :ISessionCallback{

    companion object{
        lateinit var profile_image : String
    }
    // session정보 없어서, 다시 로그인 버튼 보이는 상태
    override fun onSessionOpenFailed(exception: KakaoException?) {
        if(exception !=null){
            Logger.e(exception)
        }
    }

    // acccestoken 을 가진 상태 , 로그인후 다음 화면 진입가능
    override fun onSessionOpened() {
        redirectSignupActivity()
    }


    private fun redirectSignupActivity() {
        Logger.e("redirectSignupActivity")
        requestDetail()
    }

    fun requestDetail() {
        var keys:ArrayList<String> = ArrayList()
        keys.add("properties.nickname")
        keys.add("properties.profile_image")
        keys.add("kakao_account.age_range")
        keys.add("kakao_account.gender")
        keys.add("kakao_account.email")

        UserManagement.getInstance().me(keys,object: MeV2ResponseCallback(){
            override fun onFailure(errorResult: ErrorResult?) {
                Logger.e("가져오기 실패!")
            }

            override fun onSuccess(result: MeV2Response?) {
                    Logger.e("카카오 정보",result.toString())

                    // 카카오에서 주는 정보 아이템 세팅
                    KakaoItem().setData(result)
                    profile_image = result!!.profileImagePath

                    //동적동의 시작!
                    Logger.e("동적동의 시작!")
                    newReqAgreement(result)
            }

            // 재로그인 , 토근 새로 발급 필요!
            override fun onSessionClosed(errorResult: ErrorResult?) {
                Logger.e("세션종료!!")
            }

        })
    }

    private fun newReqAgreement(kakaoResponse: MeV2Response?) {

        var kakaoAccount = kakaoResponse!!.kakaoAccount

        var neededScopes : ArrayList<String> = ArrayList()
        if(kakaoAccount.emailNeedsAgreement() == OptionalBoolean.TRUE){
            neededScopes.add("account_email")
        }

        if(kakaoAccount.genderNeedsAgreement() == OptionalBoolean.TRUE){
            neededScopes.add("gender")
        }

        if(kakaoAccount.ageRangeNeedsAgreement() == OptionalBoolean.TRUE){
            neededScopes.add("age_range")
        }



        Session.getCurrentSession().updateScopes(mainInstance,neededScopes, object :AccessTokenCallback(){
            override fun onAccessTokenFailure(errorResult: ErrorResult?) {
                // 동의 얻기 실패
                Logger.e("동의 얻기 실패!!")
                Logger.e(errorResult!!.errorMessage)
            }

            override fun onAccessTokenReceived(accessToken: AccessToken?) {
                // 동의 요청 및 토큰 재발급
                Logger.e("onAccessTokenReceived===>"+accessToken.toString())
                // extraData 확인 - 학원코드
                if(extraDatahas()){
                    goLoginExtraFragment(kakaoResponse)
                }else{
                    // 토큰받고 메인화면 진입
                    goMainFragment(kakaoResponse)
                }
            }
        })
    }

    private fun extraDatahas(): Boolean {
        // 데이터값 있으면 바로 메인화면 진입
        return true
    }

    private fun goLoginExtraFragment(kakaoResponse: MeV2Response?) {
        mainInstance.goLoginExtraFragment(kakaoResponse)
    }

    fun goMainFragment(kakaoResponse: MeV2Response?) {
        mainInstance.goMainFragment(kakaoResponse)
    }

}
