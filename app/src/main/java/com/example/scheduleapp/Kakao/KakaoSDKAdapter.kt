package com.example.scheduleapp.Kakao

import android.content.Context
import com.example.scheduleapp.GLobalApplicaion
import com.example.scheduleapp.GLobalApplicaion.Companion.instance
import com.kakao.auth.*

class KakaoSDKAdapter : KakaoAdapter() {
    override fun getSessionConfig(): ISessionConfig {
        return object :ISessionConfig{
            // email 아이디를 저장할지
            override fun isSaveFormData(): Boolean {
                return true
            }

            override fun getAuthTypes(): Array<out AuthType>{
                return arrayOf(AuthType.KAKAO_LOGIN_ALL)
            }

            // token 저장시 암호화 여부
            override fun isSecureMode(): Boolean {
                return false
            }

            // 상관x
            override fun getApprovalType(): ApprovalType? {
                return ApprovalType.INDIVIDUAL
            }

            // 웹뷰타이머 설정할지
            override fun isUsingWebviewTimer(): Boolean {
                return false
            }
        }
    }

    override fun getApplicationConfig(): IApplicationConfig {
        return object:IApplicationConfig{
            override fun getApplicationContext(): Context {
                return instance.context()
            }
        }
    }


}
