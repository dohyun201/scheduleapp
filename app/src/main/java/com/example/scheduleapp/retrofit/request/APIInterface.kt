package com.example.scheduleapp.retrofit.request

import com.example.scheduleapp.Data.GuestItem
import com.example.scheduleapp.retrofit.requestModel.AttendModel
import com.example.scheduleapp.retrofit.response.ClassListResponse
import com.example.scheduleapp.retrofit.response.NormalResponse
import com.example.scheduleapp.retrofit.response.StoreResponse
import com.example.scheduleapp.retrofit.requestModel.StoresModel
import com.example.scheduleapp.retrofit.requestModel.LectureModel
import com.example.scheduleapp.retrofit.response.AttendlistResponse
import retrofit2.Call
import retrofit2.http.*

interface APIInterface {

    // store 저장
    @Headers("Content-Type: application/json")
    @POST("setStore")
    fun setNewStore(@Body body: StoresModel): Call<StoresModel>

    // store 목록 검색 - 중복검사
    @Headers("Content-Type: application/json")
    @POST("hasList")
    fun getStoreList(@Body body: StoresModel): Call<StoreResponse>

    @Headers("Content-Type: application/json")
    @POST("setGuest")
    fun addNewGuest(@Body body: GuestItem): Call<NormalResponse>

    // class 목록 검색
    @Headers("Content-Type: application/json")
    @POST("getClassList")
    fun getClassList(@Body body: StoresModel): Call<ClassListResponse>

    // class 참석
    @Headers("Content-Type: application/json")
    @POST("attendClass")
    fun attendClass(@Body body: AttendModel): Call<NormalResponse>

    // class 수정
    @Headers("Content-Type: application/json")
    @PUT("modifyClass")
    fun modifyClass(@Body body: LectureModel):  Call<NormalResponse>

    // class 개설
    @Headers("Content-Type: application/json")
    @POST("addClass")
    fun addNewClass(@Body body: LectureModel):  Call<NormalResponse>

    // 학생수 검색
    @Headers("Content-Type: application/json")
    @POST("getAttendList")
    fun getAttendCount(@Body body: AttendModel): Call<AttendlistResponse>

    // 수업참석 취소
    @Headers("Content-Type: application/json")
    @HTTP(method = "DELETE", path = "/cancelAttend", hasBody = true)
    fun cancelAttend(@Body body: AttendModel): Call<NormalResponse>
}