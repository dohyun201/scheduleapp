package com.example.scheduleapp.retrofit.request

import android.util.Log
import com.example.scheduleapp.retrofit.requestModel.AttendModel
import com.example.scheduleapp.retrofit.requestModel.StoresModel
import com.example.scheduleapp.retrofit.requestModel.LectureModel
import com.example.scheduleapp.retrofit.response.AttendlistResponse
import com.example.scheduleapp.retrofit.response.ClassListResponse
import com.example.scheduleapp.retrofit.response.NormalResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class APIRequest(var retrofit: APIInterface) {

    //수업 리스트 가져오기
    fun getClassList(type:Int, model: StoresModel, resultCallback : RetrofitDataResponse) {
        retrofit.getClassList(model).enqueue(object : Callback<ClassListResponse> {
            override fun onFailure(call: Call<ClassListResponse>, t: Throwable) {
                Log.e("Throwable22===>", t.message)
            }
            override fun onResponse(call: Call<ClassListResponse>, response: Response<ClassListResponse>) {
                Log.e("response22===>", response.body().toString())
                resultCallback.responseClassListResult(response.body())
            }
        })
    }

    // 수업참여
    fun attendClass(model: AttendModel, resultCallback : RetrofitDataResponse) {
        retrofit.attendClass(model).enqueue(object : Callback<NormalResponse> {
            override fun onFailure(call: Call<NormalResponse>, t: Throwable) {
                Log.e("Throwable22===>", t.message)
            }

            override fun onResponse(call: Call<NormalResponse>, response: Response<NormalResponse>) {
//                Log.e("response22===>", response.body().toString())
                resultCallback.responseResult(response.body())
            }
        })
    }

    // 수업 내용변경
    fun modifyClass(model: LectureModel, resultCallback : RetrofitDataResponse){
        retrofit.modifyClass(model).enqueue(object : Callback<NormalResponse> {
            override fun onFailure(call: Call<NormalResponse>, t: Throwable) {
                Log.e("Throwable22===>", t.message)
            }

            override fun onResponse(call: Call<NormalResponse>, response: Response<NormalResponse>) {
                Log.e("response22===>", response.body().toString())
                resultCallback.responseResult(response.body())
            }
        })
    }

    // 수업 개설
    fun addClass(model: LectureModel, resultCallback: RetrofitDataResponse) {
        retrofit.addNewClass(model).enqueue(object : Callback<NormalResponse> {
            override fun onFailure(call: Call<NormalResponse>, t: Throwable) {
                Log.e("Throwable22===>", t.message)
            }

            override fun onResponse(call: Call<NormalResponse>, response: Response<NormalResponse>) {
                Log.e("response22===>", response.body().toString())
                resultCallback.responseResult(response.body())
            }
        })
    }

    fun getAttendCount(moDel: AttendModel, resultCallback: RetrofitDataResponse) {
        retrofit.getAttendCount(moDel).enqueue(object : Callback<AttendlistResponse> {
            override fun onFailure(call: Call<AttendlistResponse>, t: Throwable) {
                Log.e("Throwable22===>", t.message)
            }
            override fun onResponse(call: Call<AttendlistResponse>, response: Response<AttendlistResponse>) {
                Log.e("response22===>", response.body().toString())
                resultCallback.responseAttendListResult(response.body())
            }
        })
    }

    fun cancelAttend(moDel: AttendModel, resultCallback: RetrofitDataResponse) {
        retrofit.cancelAttend(moDel).enqueue(object : Callback<NormalResponse> {
            override fun onFailure(call: Call<NormalResponse>, t: Throwable) {
                Log.e("Throwable22===>", t.message)
            }
            override fun onResponse(call: Call<NormalResponse>, response: Response<NormalResponse>) {
                Log.e("response22===>", response.body().toString())
                resultCallback.responseResult(response.body())
            }
        })
    }
}