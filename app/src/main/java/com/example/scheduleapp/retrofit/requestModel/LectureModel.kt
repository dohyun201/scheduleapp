package com.example.scheduleapp.retrofit.requestModel

data class LectureModel (
                              var classCode:String,
                              var aboutClass:String,
                              var className:String,
                              var classDate:String,
                              var lectureCount:Int,
                              var attendCount:Int,
                              var storeCode:Long
                              )
