package com.example.scheduleapp.retrofit.requestModel

data class AttendModel (
                        var lectureCode : String,
                        var className : String,
                        var storeCode : Long
                    )