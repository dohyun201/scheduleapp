package com.example.scheduleapp.retrofit.request

import com.example.scheduleapp.retrofit.requestModel.AttendModel
import com.example.scheduleapp.retrofit.response.AttendlistResponse
import com.example.scheduleapp.retrofit.response.ClassListResponse
import com.example.scheduleapp.retrofit.response.NormalResponse

interface RetrofitDataResponse {

    fun responseClassListResult(body: ClassListResponse?)

    fun responseResult(body: NormalResponse?)

    fun responseAttendListResult(body: AttendlistResponse?)

}
