package com.example.scheduleapp.retrofit.response

import com.example.scheduleapp.retrofit.requestModel.AttendModel

data class AttendlistResponse (var result : Boolean, var msg: String, var data:List<AttendModel>)