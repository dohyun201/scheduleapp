package com.example.scheduleapp.retrofit.init

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class APIClient{

    companion object{
        var BASEURL = "http://192.168.30.54:8080/"
    }
    lateinit var retrofit : Retrofit

    fun getClient() : Retrofit{
        // 로깅
        var interceptors : HttpLoggingInterceptor  = HttpLoggingInterceptor()
        interceptors.setLevel(HttpLoggingInterceptor.Level.BODY)
//        var client : OkHttpClient = OkHttpClient().newBuilder().addInterceptor(interceptor).build()

        // 헤더에 content-type 추가
        var clienBuilder :OkHttpClient.Builder = OkHttpClient.Builder()
        clienBuilder.addInterceptor(object : Interceptor{
                override fun intercept(chain: Interceptor.Chain): Response {
                    var requestBuilder : Request.Builder = chain.request().newBuilder()
                    requestBuilder.header("Content-Type","application/json")
                    return chain.proceed(requestBuilder.build())
                }
            })

        var client:OkHttpClient = clienBuilder.addInterceptor(interceptors).build()
        retrofit = Retrofit.Builder()
            .baseUrl(BASEURL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        return retrofit
    }
}
