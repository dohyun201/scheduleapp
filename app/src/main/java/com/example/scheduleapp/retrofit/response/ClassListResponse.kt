package com.example.scheduleapp.retrofit.response

import com.example.scheduleapp.retrofit.requestModel.LectureModel

data class ClassListResponse (var result : Boolean, var msg: String, var data:ArrayList<LectureModel>)