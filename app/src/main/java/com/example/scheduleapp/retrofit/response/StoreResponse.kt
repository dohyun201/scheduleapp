package com.example.scheduleapp.retrofit.response

import com.example.scheduleapp.retrofit.requestModel.StoresModel

data class StoreResponse (var result : Boolean, var msg: String, var data:List<StoresModel>)