package com.example.scheduleapp.Data

import android.content.Context
import com.kakao.usermgmt.response.MeV2Response
import org.json.JSONObject

var kakaoData: MeV2Response? = null

class KakaoItem(){

    fun setData(result: MeV2Response?) {
        kakaoData = result!!
    }

    fun getData(): MeV2Response? {
        return kakaoData
    }

}