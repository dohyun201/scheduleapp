package com.example.scheduleapp.Data

import com.kakao.usermgmt.response.model.AgeRange
import com.kakao.usermgmt.response.model.Gender

class GuestItem(
    var guestCode : Long,
    var storeCode:Long,
    var nickname:String,
    var email:String,
    var age_range: AgeRange,
    var gender: Gender?
) {

}