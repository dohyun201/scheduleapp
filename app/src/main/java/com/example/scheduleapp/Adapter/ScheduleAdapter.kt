package com.example.scheduleapp.Adapter

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.drawable.toDrawable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.scheduleapp.Data.KakaoItem
import com.example.scheduleapp.Holder.SceduleViewHolder
import com.example.scheduleapp.R
import com.example.scheduleapp.View.BasicInputPopup
import com.example.scheduleapp.retrofit.request.APIInterface
import com.example.scheduleapp.retrofit.request.APIRequest
import com.example.scheduleapp.retrofit.request.RetrofitDataResponse
import com.example.scheduleapp.retrofit.requestModel.AttendModel
import com.example.scheduleapp.retrofit.requestModel.StoresModel
import com.example.scheduleapp.retrofit.requestModel.LectureModel
import com.example.scheduleapp.retrofit.response.AttendlistResponse
import com.example.scheduleapp.retrofit.response.ClassListResponse
import com.example.scheduleapp.retrofit.response.NormalResponse
import com.kakao.util.helper.log.Logger
import kotlinx.android.synthetic.main.schedule_list.view.*

class ScheduleAdapter(
    var context: Fragment, var type: Int, var retrofit:APIInterface,
    var schedulelist: List<LectureModel>
) : RecyclerView.Adapter<SceduleViewHolder>() {

    var TEACHER = 1
    var CLIENT = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SceduleViewHolder {
        val inflateView = LayoutInflater.from(parent.context).inflate(R.layout.schedule_list,parent,false)
        return SceduleViewHolder(inflateView)
    }

    override fun getItemCount(): Int {
        return schedulelist.size
    }

    override fun onBindViewHolder(holder: SceduleViewHolder, position: Int) {

        var classTime = schedulelist[position].classDate

        var className = schedulelist[position].className
//        var date = schedulelist[position].date
//        var days = schedulelist[position].days

        var classTitle = schedulelist[position].className
        var classContents = schedulelist[position].aboutClass
        var classCount = schedulelist[position].lectureCount

        var attendCount = schedulelist[position].attendCount

        var attendCount = schedulelist[position].attendCount

        setInitView(holder)

//        holder.itemView.date.text = date
//        holder.itemView.days.text = days

//        Log.e("classTime",classTime.substring(8))
        holder.itemView.date.text = classTime.substring(8,10)
        holder.itemView.days.text = classTime.substring(10)

        holder.itemView.class_name.text = className
        // 수업명
        holder.itemView.about_class_title.text = classTitle
        // 수업내용
        holder.itemView.about_class_contents.text = classContents
        // 참석인원수
        holder.itemView.regist_count.text = ""+ attendCount + "/ "+classCount+"명"

        holder.itemView.setOnClickListener(object: View.OnClickListener{
            override fun onClick(view: View?) {
                Logger.e(className)
                if(holder.itemView.about_class_card.visibility == View.VISIBLE){
                    holder.itemView.about_class_card.visibility = View.GONE
                }else{
                    holder.itemView.about_class_card.visibility = View.VISIBLE

                }
            }
        })

        holder.itemView.regist_class =

        // 이미 참여했다면
        holder.itemView.regist_class.isChecked = false

        // 수업참석
        holder.itemView.regist_class.setOnClickListener(object : View.OnClickListener{
            override fun onClick(view: View?) {

                if(type == CLIENT){
                    // 학생입장
                    // pk = classcode+A(구분자)+kakaoid값
                    var kakao = KakaoItem().getData()
                    var moDel = AttendModel(
                        schedulelist[position].classCode+"A"+kakao!!.id.toInt(),
                        schedulelist[position].className,
                        schedulelist[position].storeCode
                     )

                    if(holder.itemView.regist_class.isChecked){
                        // client는 수업 선택후 참석, DB 등록

                        holder.itemView.regist_class.background = Color.parseColor("#64d8cb").toDrawable()

                        Log.e("schedulelist==>", schedulelist[position].className)
                        APIRequest(retrofit).attendClass(moDel, object : RetrofitDataResponse {
                            override fun responseAttendListResult(body: AttendlistResponse?) {
                            }

                            override fun responseClassListResult(body: ClassListResponse?) {

                            }

                            override fun responseResult(body: NormalResponse?) {
                                Log.e("response22===>", body.toString())
                                //참석자수 체킹
                                getattendCount(moDel)
                            }
                        })
                    }else{
                        holder.itemView.regist_class.background = Color.parseColor("#ffffff").toDrawable()
                        // 참석 취소
                        APIRequest(retrofit).cancelAttend(moDel, object : RetrofitDataResponse {
                            override fun responseAttendListResult(body: AttendlistResponse?) {
                            }

                            override fun responseClassListResult(body: ClassListResponse?) {}

                            override fun responseResult(body: NormalResponse?) {
                                Log.e("response22===>", body.toString())
                                //수업 참석 취소 완료 및 참석자수 업데이트
                                getattendCount(moDel)
                            }
                        })
                    }
                }

            }
        })

        // 수업수정
        holder.itemView.modify_class.setOnClickListener(object : View.OnClickListener{
            override fun onClick(view: View?) {

                Log.e("schedulelist==>", schedulelist[position].toString())
                modifyClass(schedulelist[position])
            }
        })

    }

    private fun getattendCount(moDel: AttendModel) {
        APIRequest(retrofit).getAttendCount(moDel, object : RetrofitDataResponse {
            override fun responseAttendListResult(body: AttendlistResponse?) {
                Log.e("responseAttend===>", body!!.data.size.toString())
                // 일치하는 클래스 찾아서 해당 참석차수 업데이트
                var attendCount = body!!.data.size
                searchClass(moDel,attendCount)
            }

            override fun responseClassListResult(body: ClassListResponse?) {
            }

            override fun responseResult(body: NormalResponse?) {
                Log.e("response22===>", body.toString())
            }
        })
    }

    private fun searchClass(moDel: AttendModel, count: Int) {
        APIRequest(retrofit).getClassList(type, StoresModel(moDel.storeCode,""), object : RetrofitDataResponse{
            override fun responseAttendListResult(body: AttendlistResponse?) {
            }

            override fun responseResult(body: NormalResponse?) {

            }

            override fun responseClassListResult(response: ClassListResponse?) {
                Log.e("개설된 클래스들=>","getClassList ===> "+ response)
                // 여기서 해당하는 클래스 가려서, 업데이트
                updateClassCount(moDel,response,count)
            }
        })
    }
//
    private fun updateClassCount(
        requestModel: AttendModel,
        responseModel: ClassListResponse?,
        count: Int
    ) {
        for(model in responseModel!!.data){
            if(model.classCode.equals(requestModel.lectureCode.substringBefore("A"))){
                //그떄의 모델에 새로운 카운트를 업데이트
                model.attendCount = count

                APIRequest(retrofit).modifyClass(model, object : RetrofitDataResponse {
                    override fun responseAttendListResult(body: AttendlistResponse?) {
                    }

                    override fun responseClassListResult(body: ClassListResponse?) {}

                    override fun responseResult(body: NormalResponse?) {
                        // 화면 갱신
                        Log.e("업데이트","업데이트 완료!!!")
//                        context.onResume()
                    }
                })
            }
        }

    notifyDataSetChanged()
    }


    private fun modifyClass(classModel: LectureModel) {
        BasicInputPopup(this,retrofit,context,classModel,"modify").show()
    }

    fun setInitView(view: SceduleViewHolder) {
        if(type == TEACHER){
            view.itemView.regist_class.visibility = View.GONE
        }else{
            view.itemView.modify_class.visibility = View.GONE
        }
    }
}